﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class enemy : MonoBehaviour {

    private GameObject player;                                                                      // holds player game object
    private List<int[]> playerPath;                                                                 // stores player path
    private int[] currentCell;
    private int pathCounter;                                                                        // stores current location in player path
    private bool hunting;                                                                           // true - use FollowPlayer(), false - use FollowPath()
    private bool isMoving;                                                                          // true - Coroutine is running, false - not running
    private float movingSpeed;                                                                      // speed of enemy, needed to go faster when hunting

	void Start () {
        hunting = false;
        isMoving = false;

        currentCell = new int[2] { 0, 0 };
        playerPath = new List<int[]>();

        pathCounter = 0;
        movingSpeed = 0.03f;
        player = GameObject.Find("Haruko");
        UpdatePath();

	}
	
	void Update () {

        PathOrPlayer();

	}

    private void PathOrPlayer()
    {
        if (hunting)
        {
            FollowPlayer();
        }


        if (!hunting)
        {            
            FollowPath();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Haruko")
        {
            Debug.Log("Trigger Enter");
            movingSpeed = 0.08f;
            hunting = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Haruko")
        {
            Debug.Log("Trigger Exit");
            movingSpeed = 0.03f;
            UpdatePath();
            pathCounter = playerPath.Count - 2;
            hunting = false;
        }
    }

    /// <summary>
    /// Function moves enemy to next cell of player path
    /// </summary>
    private bool FollowPath()
    {
        // check for Coroutine state
        if(isMoving)
        {
            return false;
        }

        int x;
        int y;

        if (playerPath.Count - pathCounter < 5)
        {
            UpdatePath();
        }

        // check for playerPath length, does not let enemy go over playerPath
        if(pathCounter >= playerPath.Count)
        {
            return false;
        }

        x = playerPath[pathCounter][0];
        y = playerPath[pathCounter][1];
        
        StartCoroutine(Movement( new Vector3(x * 2.5f + 1.25f, y * 2.5f + 1.25f, -0.5f) ));

        return false;
    }

    /// <summary>
    /// Function moves enemy closer to player
    /// and based on X and Y coordinates difference
    /// decides which way to go first
    /// </summary>
    private bool FollowPlayer()
    {

        // check for Coroutine state
        if (isMoving)
        {
            return false;
        }

        int X, Y;
        int[] playerCell;

        playerCell = player.GetComponent<player>().CurrentCell;

        X = playerCell[0] - currentCell[0];
        Y = playerCell[1] - currentCell[1];

        if(Mathf.Abs(X) < Math.Abs(Y))
        {
           Y = Y > 0 ? 1 : Y < 0 ? -1 : 0 ;
            StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f, (currentCell[1] + Y) * 2.5f + 1.25f, -0.5f)));
            return false;
        }

        if(Mathf.Abs(X) > Mathf.Abs(Y))
        {
            X = X > 0 ? 1 : X < 0 ? -1 : 0;
            StartCoroutine(Movement(new Vector3((currentCell[0] + X) * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f, -0.5f)));
            return false;
        }

        if(Mathf.Abs(X) == Mathf.Abs(Y))
        {
            X = X > 0 ? 1 : X < 0 ? -1 : 0;
            StartCoroutine(Movement(new Vector3((currentCell[0] + X) * 2.5f + 1.25f , currentCell[1] * 2.5f + 1.25f, -0.5f)));
            return false;
        }

        return false;
    }

    IEnumerator Movement(Vector3 target)
    {
        isMoving = true;

        transform.LookAt(target, Vector3.forward);
        while (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, movingSpeed);
            yield return new WaitForSeconds(0.01f);
        }

        currentCell[0] = (int)((target.x - 1.25f) / 2.5f);
        currentCell[1] = (int)((target.y - 1.25f) / 2.5f);
        pathCounter++;
        isMoving = false;
        yield return null;
    }

    /// <summary>
    /// Function updates variable playerPath,
    /// it takes new path from player
    /// </summary>
    private void UpdatePath()
    {
        playerPath = player.GetComponent<player>().PlayerPath;
      
    } 
}
