﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Linq;

public class player : MonoBehaviour
{

    private Animator animator;                                                                  // Animator
    private String[,] dirMap;                                                                   // stroes the arrow map
    private int[] currentCell;                                                                  // stores current location of player
    private bool isMoving;                                                                      // boolean for checking if corouting running at the moment
    private List<int[]> playerPath = new List<int[]>();
    private string backPath;
    private string frontPath;
    StringBuilder builderBack = new StringBuilder();
    StringBuilder builderFront = new StringBuilder();
    public int count;
    private int step;

    void Start()
    {
        dirMap = null;
        animator = GetComponent<Animator>();
        currentCell = GameObject.Find("Maker").GetComponent<create>().StartNode;
        isMoving = false;

        transform.position = new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f, 0);
    }

    private void Update()
    {

    }

    void FixedUpdate()
    {

        float Horiz = Input.GetAxis("Horizontal");
        float Vert = Input.GetAxis("Vertical");

        if (Horiz > 0.1f)
        {
            MovePlayer(direction.RIGHT);
        }

        if (Horiz < -0.1f)
        {
            MovePlayer(direction.LEFT);
        }

        if (Vert > 0.1f)
        {
            MovePlayer(direction.UP);
        }

        if (Vert < -0.1f)
        {
            MovePlayer(direction.DOWN);
        }

    }

    public enum direction
    {
        UP,
        RIGHT,
        DOWN,
        LEFT,
    }

    public String[,] DirMap
    {
        get
        {
            return dirMap;
        }
        set
        {
            dirMap = value;
        }
    }

    public List<int[]> PlayerPath
    {
        get
        {
            return playerPath;
        }
    }

    public int[] CurrentCell
    {
        get
        {
            return currentCell;
        }
    }

    /// <summary>
    /// Function checks map restrictions for movement
    /// and checks for current movement
    /// </summary>
    /// <param name="dir"></param>
    private void MovePlayer(direction dir)
    {
        switch (dir)
        {
            case direction.UP:

                if (CanMove('0') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(-90, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f + 2.5f, 0)));
                }
                break;

            case direction.RIGHT:

                if (CanMove('1') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(0, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f + 2.5f, currentCell[1] * 2.5f + 1.25f, 0)));
                }
                break;

            case direction.DOWN:

                if (CanMove('2') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(90, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f - 2.5f, 0)));
                }
                break;

            case direction.LEFT:

                if (CanMove('3') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(180, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f - 2.5f, currentCell[1] * 2.5f + 1.25f, 0)));
                }
                break;

            default:
                break;

        }
    }

    //private void GetSequence()
    //{
    //    if (playerPath.Count > 7)
    //    {
    //        if (backPath == null && frontPath == null)
    //        {
    //            for (int i = (playerPath.Count - 2); i >= 5; i--)
    //            {
    //                if (playerPath[i] == playerPath[playerPath.Count - 1])
    //                {
    //                    for (int j = 0; j <= 5; j++)
    //                    {
    //                        var bch = playerPath[i - j];
    //                        builderBack.Append(bch);
    //                        if (playerPath.Count >= i + j + 1)
    //                        {
    //                            var fch = playerPath[i + j];
    //                            builderFront.Append(fch);
    //                        }

    //                    }
    //                    backPath = builderBack.ToString();
    //                    frontPath = builderFront.ToString();
    //                    step = 1;
    //                    builderBack.Length = 0;
    //                    builderFront.Length = 0;
    //                }
    //            }
    //        }
    //        else
    //        {
    //            string backPathString = backPath[backPath.Length - step].ToString() + backPath[backPath.Length - step - 1].ToString();
    //            while(step < backPath.Length)
    //            {
    //                if (playerPath [playerPath.Count - 1] == backPathString)
    //                {
    //                    step++;
    //                }
    //                else
    //                {
    //                    step = 0;
    //                }
    //            }

    //        }
    //    }
    //}

    IEnumerator Movement(Vector3 target)
    {
        isMoving = true;
        playerPath.Add(currentCell.ToArray());
        //GetSequence();
        animator.SetBool("Walk", true);

        while (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, 0.1f);
            yield return new WaitForSeconds(0.01f);
        }

        currentCell[0] = (int)((target.x - 1.25f) / 2.5f);
        currentCell[1] = (int)((target.y - 1.25f) / 2.5f);

        animator.SetBool("Walk", false);
        isMoving = false;
        yield return null;

    }

    /// <summary>
    /// Function check allowed directions for player in current cell.
    /// true - movement is allowed,
    /// false - movement is forbidden.
    /// </summary>
    /// <param name="turn"></param>
    /// <returns></returns>
    private bool CanMove(char turn)
    {
        foreach (char arrow in dirMap[currentCell[0], currentCell[1]])
        {
            if ((turn).Equals(arrow))
            {
                return true;
            }
        }

        return false;
    }
}
